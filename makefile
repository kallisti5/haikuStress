CC=gcc
DEBUG='-g'

all: memeater pistress

pistress: pistress.c
	$(CC) $(DEBUG) -o $@ $^

memeater: memeater.c
	$(CC) $(DEBUG) -o $@ $^

clean:
	rm -f pistress memeater
