/*
 * Copyright, 2013 Alexander von Gluck IV
 * Released under the terms of the MIT license
 */


#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef __HAIKU__
#include <OS.h>
#define USE_AREA 1
#else
#include "be_compat.h"
#endif


#define MEGABYTE 1048576


int
main(int argc, char *argv[])
{
	if (argc != 3) {
		printf("%s [size (MB)] [count]\n", argv[0]);
		return 0;
	}

	uint32 size = atoi(argv[1]) * MEGABYTE;
	uint32 count = atoi(argv[2]);

	printf("Creating and using memory that is %d MB, %d times...\n",
		size / MEGABYTE, count);

	srand(time(NULL));
	int i = 0;
	for (i = 0; i < count; i++) {
		// Randomize based on random number to help reduce identical seeds
		srand(rand());
		int32 random = (rand() % 16) + 1;
		int32 randomBlock = (B_PAGE_SIZE * random) + random;

		printf("Round %d: Creating.. ", i);
		void* location = NULL;
		#ifdef USE_AREA
		area_id area = create_area("Test Memory", &location, B_ANY_ADDRESS, size,
			B_NO_LOCK, B_READ_AREA | B_WRITE_AREA);
		#else
		location = (void*)malloc(size);
		void* mallocLocation = location;
		#endif

		if (!location) {
			printf("FAIL!\n");
			continue;
		}
		printf("Created(%016p).. ", location);

		printf("Exercising.. ");
		addr_t pos = 0;
		while (pos < size - randomBlock) {
			uint32 data = randomBlock / random;
			memset(location, data, randomBlock);
			location += randomBlock;
			pos += randomBlock;
		}
		printf("Erasing.. ");
		#ifdef USE_AREA
		delete_area(area);
		#else
		free(mallocLocation);
		#endif
		printf("OK!\n");
	}

	return 0;
}
