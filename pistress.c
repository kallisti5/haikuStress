/*
 * Copyright, 2013 Alexander von Gluck IV
 * Released under the terms of the MIT license.
 */


#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <OS.h>


#define PRIORITY B_NORMAL_PRIORITY


status_t
calculatepi(void* pointer)
{
	/* Dik T. Winter, CWI Amsterdam */
	/* Calculate 800 digits of Pi */
	int a = 10000;
	int b = 0;
	int c = 2800;
	int d = 0;
	int e = 0;
	int f[2801];
	memset(f, 0, sizeof(f));
	int g = 0;

	for(;b-c;)
		f[b++] = a / 5;
	for(;d = 0, g = c * 2; c -= 14, e + d / a, e = d%a)
		for(b = c; d += f[b] * a, f[b] = d% --g, d /= g--, --b; d *= b);

	return B_OK;
}


int
main(int argc, char *argv[])
{
	if (argc != 3) {
		printf("Generate Pi like a boss\n");
		printf("%s [threads] [count]\n", argv[0]);
		return 0;
	}

	int threadCount = atoi(argv[1]);
	int piCount = atoi(argv[2]);

	printf("Starting %d rounds of Pi over %d cpus\n",
		piCount, threadCount);

	int i = 0;
	for (i = 0; i < piCount; i++) {
		printf("Round %d: (", i);

		thread_id threads[threadCount];

		memset(threads, 0, sizeof(threads));

		int threadID = 0;
		for (threadID = 0; threadID < threadCount; threadID++) {
			printf("+");
			char threadName[32];
			snprintf(threadName, sizeof(threadName),
				"PiThread %d", threadID);
			threads[threadID] = spawn_thread(calculatepi,
				threadName, PRIORITY, threadName);
			resume_thread(threads[threadID]);
		}

		/* wait for threads to complete */
		for (threadID = 0; threadID < threadCount; threadID++) {
			status_t result;
			wait_for_thread(threads[threadID], &result); 
			printf("-");
		}
		printf(")\n");
	}
	return 0;
}
